﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showCollection : MonoBehaviour {
	public GameObject textHolder;
	public GameObject theController;
	public Text theText;
	public GameObject collectionHolder;
	// Use this for initialization
	void Start () {
		theController = GameObject.Find("controls");
		theText = textHolder.GetComponent<Text> ();
		Debug.Log (theText.text + "here");
		theText.text = "Total Packs Collected: " + theController.GetComponent<collectionCounter> ().totalCollected + "\n\n";
		getText (); 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void getText()
	{
		string textString = "";
		int Counter = 0;
		for(var i=0; i<theController.GetComponent<collectionCounter> ().groupLists.Length; i++)
		{
			
			for(var j=0; j<theController.GetComponent<collectionCounter> ().groupLists[i].got.Length; j++)
			{
				Counter++;
				//Debug.Log (i + "hi" + j);
				GameObject holder = Instantiate(collectionHolder, new Vector3 (0,0,0), Quaternion.identity);
				holder.transform.SetParent(textHolder.transform,false);
				int tempY = (-165 * Counter)+3550;//set the coords properly. no i don't know why they have gone stupid.
				//put text in the box
				//change colour if got
				holder.transform.localPosition = new Vector3 (-20, tempY, 0);
				holder.transform.localScale = new Vector3 (1, 1, 1);
				textString = "";
				textString = textString + "" + theController.GetComponent<collectionCounter> ().groupLists [i].packetNames[j];
				if(theController.GetComponent<collectionCounter> ().groupLists [i].got[j])
				{
					textString = textString + " GOT!!!!";
					holder.transform.GetComponentInChildren<Image> ().color = Color.red;
				} 
				textString = textString + "\n" + "Bummer Rating = " + theController.GetComponent<collectionCounter> ().groupLists [i].bummerRating [j];
				textString = textString + "\n" + "Gross Rating = " + theController.GetComponent<collectionCounter> ().groupLists [i].grossRating[j];
				if(theController.GetComponent<collectionCounter> ().groupLists [i].ratingNotes[j]!="")
				{
					textString = textString + "\n" + "Notes = " + theController.GetComponent<collectionCounter> ().groupLists [i].ratingNotes[j];
				}
				textString = textString + "\n" + "Number Collected = " + theController.GetComponent<collectionCounter> ().groupLists [i].numCollected [j];
				holder.transform.GetComponentInChildren<Text> ().text= textString;
				//Debug.Log (textString);
			
			}

		}
	}
}
