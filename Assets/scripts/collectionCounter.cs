﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collectionCounter : MonoBehaviour {
	public int totalCollected;
	public int foundPacketGroupNum = 0;
	public int foundPacketGroupID = 0;
	public bool isTracking;
	public int currentGroupNum;
	public int currentGroupID;
	public GameObject textHolder;
	[System.Serializable]
	public class GroupList
	{
		public string[] packetNames;
		public bool[] got;
		public int[] bummerRating;
		public int[] grossRating;
		public string[] ratingNotes;
		public int[] numCollected;
	}
	public GroupList[] groupLists;
	// Use this for initialization
	void Start () {
		loadPlayerPrefs ();
	}
	void Awake() {
		DontDestroyOnLoad (transform.gameObject);
	}
	// Update is called once per frame
	void Update () {
		if (isTracking && ((foundPacketGroupNum != currentGroupNum) || (foundPacketGroupID != currentGroupID))) {
			Debug.Log ("track this packet " + foundPacketGroupNum + " " + foundPacketGroupID);
			currentGroupNum = foundPacketGroupNum;
			currentGroupID = foundPacketGroupID;
			if (!groupLists [foundPacketGroupNum-1].got [foundPacketGroupID-1]) {
				Debug.Log ("NEWWWWWWW!!!!");
			} else {
				Debug.Log ("already got");
			}
			savePlayerPrefs (foundPacketGroupNum,foundPacketGroupID);
		}
	}
	//onload (maybe in splash screen) check player prefs, if they exist fill in the exists groupLists[n].got[n]
	void loadPlayerPrefs()
	{
		totalCollected = PlayerPrefs.GetInt ("totalPacketsCollected");
		for(var i=0; i<groupLists.Length; i++)
		{
			for(var j=0; j<groupLists[i].got.Length; j++)
			{
				int savedValue = PlayerPrefs.GetInt ("Group" + (i+1) + "ID" + (j+1));
				if (savedValue==1) {
					groupLists [i].got [j] = true;
				} else {
					groupLists [i].got [j] = false;
				}
				groupLists [i].numCollected[j] = PlayerPrefs.GetInt ("Group" + (i+1) + "NumOf" + (j+1));
			}
		}
	}
	void savePlayerPrefs(int GroupNum, int GroupID)
	{
		
		PlayerPrefs.SetInt ("Group" + GroupNum + "ID" + GroupID, 1);
		int collectNum = PlayerPrefs.GetInt ("Group" + GroupNum + "NumOf" + GroupID);
		totalCollected =  PlayerPrefs.GetInt ("totalPacketsCollected");
		totalCollected = totalCollected + 1;
		PlayerPrefs.SetInt ("totalPacketsCollected", totalCollected);
		groupLists [GroupNum-1].got [GroupID-1] = true;
		groupLists [GroupNum-1].numCollected[GroupID-1] = collectNum + 1;
		PlayerPrefs.SetInt ("Group" + GroupNum + "NumOf" + GroupID, groupLists [GroupNum-1].numCollected[GroupID-1]);
	}

	public void gotoScene(int sceneNumber)
	{
		SceneManager.LoadScene(sceneNumber);
	}
}
