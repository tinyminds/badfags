using UnityEngine;

public class rotateToFaceCamera : MonoBehaviour {
public Camera MainCamera;

	public void Update()
	{
		Vector3 fwd = MainCamera.transform.forward;
		transform.rotation = Quaternion.LookRotation(fwd);
	}
}   

